# TAGS

## Observações

- Eu desenvolvi no Spring Tool Suite
- Estou executando com o comando 
  - spring-boot:run -DskipTests
- Estou executando com o comando no caso de passar um arquivo para o programa
  - spring-boot:run -DskipTests -Dspring-boot.run.arguments=C:\\Dev\\workspace-sysmanager\\Tags\\src\\main\\resources\\testFile.txt
- Os testes estão abrangendo somente o serviço que trata as Tags, são apenas teste unitários
- Tudo foi enviado direto para o master, não utilizei o git flow nem me importei com issues (não é assim que trabalho, mas como era apenas um teste não levei em consideração este rito)

<span style="color:red">*Geralmente não se faz Java Docs sobre funções privadas ou protegidas, mas para um melhor entendimento do avaliador eu fiz*</span>

## Questão 2

<p>Em uma linguagem baseada em tags, como XML ou HTML, o conteudo é envolto entre uma tag de
abertura e sua correspondente tag de fechamento, por exemplo &lt;tag&gt; e &lt;/tag&gt; . Toda a tag de
fechamento começa com /. Toda tag de abertura deve ter sua correspondente de fechamento e vice-versa.
<br>
Dado um texto em uma linguagem baseada em tags, escreva uma função que extraia o conteudo de
um conjunto bem definido de tags, obedecendo aos seguintes critérios:

* Os nomes das tags de entrada e saida devem ser os mesmos. O codigo html \<h1>Hello World\</h2> não é valido pois a tag de saida não é igual à de entrada.
* Tags podem ser aninhadas, mas o conteudo entre tags aninhadas não é considerado valido. Por exemplo, em \<a>conteudos\</a>invalidos\</h1> conteudos é valido mas invalidos não.
* Tags podem consistir de qualquer conjunto de caracteres imprimiveis, desde que da língua portuguesa.
* O tamanho do texto deve ser de, pelo menos, 15 caracteres e não maior que 100.000.

Exemplo de saida:

Entrada:

```code
<h1>Nayeem loves counseling</h1>
<h1><h1>Sanjay has no watch</h1></h1><par>So wait for a while</par>
<Amee>safat codes like a ninja</amee>
<SA premium>Imtiaz has a secret crush</SA premium>
```
Saída:
```code
Nayeem loves counseling
Sanjay has no watch
So wait for a while
None
Imtiaz has a secret crush
```

