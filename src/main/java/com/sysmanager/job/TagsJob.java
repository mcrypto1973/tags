package com.sysmanager.job;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import com.sysmanager.exception.TagMaxLimitException;
import com.sysmanager.exception.TagMinLimitException;
import com.sysmanager.service.TagsService;

@Component
public class TagsJob implements ApplicationRunner {
	
	@Autowired
	private TagsService tagsService;
	
	@Autowired
	ResourceLoader resourceLoader;

	/**
	 * Recebe o caminho para leitura de um arquivo ou se não receber argumento nenhum le o arquivo test.txt que se encontra no src/main/resources
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		String path = "";
		for (String arg : args.getSourceArgs()) {
			path = arg;
		}
		
		String data = "";
		
		try {
			InputStream inputStream = null;
			if (path.length() > 0) {
				File file = new File(path);
				inputStream = new FileInputStream(file);	
			}
			else {
				Resource resource = resourceLoader.getResource("classpath:testFile.txt");
				inputStream = resource.getInputStream();
			}
			
			byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
			data = new String(bdata, StandardCharsets.UTF_8);
			
			List<String> result = tagsService.processTags(data);
			System.out.println(result);
		}
		catch(TagMaxLimitException | TagMinLimitException | IOException ex) {
			System.out.println(ex.getMessage());
		}
		
	}
	
}
