package com.sysmanager.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.sysmanager.exception.TagMaxLimitException;
import com.sysmanager.exception.TagMinLimitException;

/**
 * 
 * @author Marcio Cruz de Almeida
 *
 */
@Service
public class TagsService {
	
	private static final int MIN_TEXT = 15;
	private static final int MAX_TEXT = 100000;
	
	/**
	 * 
	 * @param text
	 * @return
	 * @throws TagMaxLimitException
	 * @throws TagMinLimitException
	 */
	public List<String> processTags(String text) throws TagMaxLimitException, TagMinLimitException {
		
		List<String> result = new ArrayList<String>();
		
		if (text.length() < MIN_TEXT) {
			throw new TagMinLimitException("Texto menor que o permitido!!!");
		}
		else if (text.length() > MAX_TEXT) {
			throw new TagMaxLimitException("Texto maior que o permitido!!!");
		}
				
		List<String> listText = getTagsAndTextData(text);
		Iterator<String> textIter = listText.iterator();
		while (textIter.hasNext()) {
			String textExtract = textIter.next();
			String tag = getTags(textExtract);

			tag = tag.replace(">", "");
			tag = tag.replace("<", "");
			String realText = getTextData(textExtract, tag);
			if (realText != "")
				result.add(realText);
			else
				result.add("None");
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	private List<String> getTagsAndTextData(String text) {
		
		Pattern regex = Pattern.compile("<[A-Za-z0-9 ]*>\\b[^>]*>(.*?)", Pattern.DOTALL);
		
	    final List<String> tagValues = new ArrayList<String>();
	    final Matcher matcher = regex.matcher(text);
	    while (matcher.find()) {
	        tagValues.add(matcher.group());
	    }
	    return tagValues;
	}

	/**
	 * 
	 * @param str
	 * @param tag
	 * @return
	 */
	private String getTextData(final String str, String tag) {
		Pattern regex = Pattern.compile("<" + tag + ">(.+?)</" + tag + ">", Pattern.DOTALL);
		
	    final Matcher matcher = regex.matcher(str);
	    while (matcher.find()) {
	        return matcher.group(1);
	    }
	    return "None";
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	private String getTags(String str) {
		Pattern regex = Pattern.compile("<(([a-zA-Z0-9 ])+?)>", Pattern.DOTALL);
		
	    final Matcher matcher = regex.matcher(str);
	    while (matcher.find()) {
	        return matcher.group(1);
	    }
	    
	    return "None";
	}

}
