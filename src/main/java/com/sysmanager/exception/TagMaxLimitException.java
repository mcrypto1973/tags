package com.sysmanager.exception;

public class TagMaxLimitException extends Exception {

	private static final long serialVersionUID = 679811479901888186L;
	
	public TagMaxLimitException(String message) {
		super(message);
	}

}
