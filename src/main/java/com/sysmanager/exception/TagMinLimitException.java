package com.sysmanager.exception;

public class TagMinLimitException extends Exception {

	private static final long serialVersionUID = -7926731185915461278L;
	
	public TagMinLimitException(String message) {
		super(message);
	}

}
