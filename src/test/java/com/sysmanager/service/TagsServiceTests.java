package com.sysmanager.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import com.sysmanager.exception.TagMaxLimitException;
import com.sysmanager.exception.TagMinLimitException;

/**
 * 
 * @author Marcio Cruz de Almeida
 *
 */
@SpringBootTest
public class TagsServiceTests {
	
	private static final String TEXT = "<h1>Nayeem loves counseling</h1>"
			+ "<h1><h1>Sanjay has no watch</h1></h1><par>So wait for a while</par>"
			+ "<Amee>safat codes like a ninja</amee>"
			+ "<SA premium>Imtiaz has a secret crush</SA premium>";
	
	private static final String TEXT_MIN = "<h1>Nayeem</h1>";
	
	@Autowired
	private TagsService tagsService;
	
	@Autowired
	ResourceLoader resourceLoader;
	
	/**
	 * Testa de o texto passado é inferior a 15 caracteres
	 */
	@Test
	void tagsInvalidMinText() {
		try {
			tagsService.processTags(TEXT_MIN);
		} catch (TagMaxLimitException | TagMinLimitException ex) {
			String expectedMessage = "Texto menor que o permitido!!!";
			String exceptionMsg = ex.getMessage();
			
			assertTrue(exceptionMsg.contains(expectedMessage));
		}
	}
	
	/**
	 * Testa de o texto passado é superior a 100000 caracteres
	 * @throws  
	 */
	@Test
	void tagsInvalidMaxText() {
		try {
			Resource resource = resourceLoader.getResource("classpath:testMaxText.txt");
			InputStream inputStream = resource.getInputStream();
			byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
			String data = new String(bdata, StandardCharsets.UTF_8);
			
			tagsService.processTags(data);
		} catch (TagMaxLimitException | TagMinLimitException | IOException ex) {
			String expectedMessage = "Texto maior que o permitido!!!";
			String exceptionMsg = ex.getMessage();
			
			assertTrue(exceptionMsg.contains(expectedMessage));
		}
	}
	
	/**
	 * Teste se o texto passado é válido
	 */
	@Test
	void tagsValidTest() {
		try {
			List<String> text = tagsService.processTags(TEXT);
			
			assertEquals(text.size(), 5);
			
			assertEquals(text.get(0), "Nayeem loves counseling");
			assertEquals(text.get(1), "Sanjay has no watch");
			assertEquals(text.get(2), "So wait for a while");
			assertEquals(text.get(3), "None");
			assertEquals(text.get(4), "Imtiaz has a secret crush");
			
		} catch (TagMaxLimitException | TagMinLimitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
}
